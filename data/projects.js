export default [
	{
		name: "HTML|CSS Profile",
		src: "/c1.jpg",
		description: "Basic Profile Web Page using HTML, CSS, Bootstrap",
		url: "https://paguejoy.gitlab.io/c1/"
	},
	{
		name: "React-Booking System",
		src: "/c2.jpg",
		description: "Course Booking App with the use of MERN Stack - MongoDB, ExpressJS, ReactJS, NodeJS",
		url: "https://paguejoy.gitlab.io/capstone2-pague/"
	},
	{
		name: "Work with me",
		src: "/work.jpg",
		description: "Please click the link below:",
		url: "/contact"
	}

]