export default [
	{
		name: "HTML",
		src: "/html.png"
	},
	{
		name: "CSS",
		src: "/css.png"
	},
	{
		name: "Bootstrap",
		src: "/bootstrap.jpg"
	},
	{
		name: "Javascript",
		src: "/javascript.png"
	},
	{
		name: "Git",
		src: "/git.png"
	},
	{
		name: "Gitlab",
		src: "/gitlab.png"
	},
	{
		name: "Node JS",
		src: "/nodejs.png"
	},
	{
		name: "Postman",
		src: "/postman.png"
	},
	{
		name: "MongoDB",
		src: "/mongodb.png"
	},
	{
		name: "React JS",
		src: "/reactjs.png"
	},
	{
		name: "Next JS",
		src: "/nextjs.png"
	},
	{
		name: "Heroku",
		src: "/heroku.png"
	},
	
];

