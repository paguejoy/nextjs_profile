import React from 'react'; 
import {Container} from 'react-bootstrap';


export default function Contact(){
	return(
		<>
			<div>
				<Container className="my-5 text-center">
					<h1>Contact the Developer</h1>
					<h4>Email: paguejoy@gmail.com</h4>
					<h4>Mobile number: +639211507728</h4>
				</Container>
			</div>

		</>

		)
}