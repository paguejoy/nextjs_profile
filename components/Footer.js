import Link from 'next/link';

export default function Footer(){
	return(

		<div className="bg-primary text-light footer d-flex justify-content-center align-items-center fixed-bottom">
			<a 
				href="https://www.facebook.com/itsmeduchessjoy" 
				target="_blank" 
				className="text-light mx-4"
			>
				<i className="fab fa-facebook footer-link" aria-hidden="true">FB</i>
			</a>
			<a 
				href="https://www.linkedin.com/in/christiana-joy-pague-05791719b/" 
				target="_blank" 
				className="text-light mx-4"
			>
				<i className="fab fa-linkedin footer-link" aria-hidden="true">LinkedIn</i>
			</a>
			<a 
				href="https://gitlab.com/paguejoy" 
				target="_blank" 
				className="text-light mx-4"
			>
				<i className="fab fa-gitlab footer-link footer-red" aria-hidden="true">Gitlab</i>
			</a>
			<Link href="/contact">
				<a className="text-light">
				<i className="fab fa-gitlab footer-link footer-red" aria-hidden="true">Contact</i>
				</a>
			</Link>
			
		</div>

		)
}